package main

import (
	"encoding/json"
	"errors"
	"fmt"
	retry "github.com/avast/retry-go"
	w "github.com/jimlawless/whereami"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	log "gitlab.com/venchoware/go/log4go"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"sync"
	"time"
)

var v *viper.Viper

func init() {
	v = viper.New()
}

func initArgs(args []string) error {
	pflag.String("site", "", "Crawl just this site")
	pflag.String("config", "smusmabot.conf", "config file")
	pflag.Int("maxbots", 20, "maximum # of bots")
	pflag.String("save", "", "save data to this directory")
	pflag.String("load", "", "load data from this directory")
	pflag.String("loglevel", "INFO", "logging level")
	pflag.Int("cache_max", 5, "Cache max")

	pflag.String("service_vendor_crawl_starts", "",
		"Service: vendor crawl starts")
	pflag.String("service_vendor_crawl_finishes", "",
		"Service: vendor crawl finishes")
	pflag.String("service_vendor_crawl_unnecessary", "",
		"Service: vendor crawl unnecessary")
	pflag.String("service_cache", "", "Service: cache")

	pflag.CommandLine.Parse(args[1:])

	v.SetEnvPrefix("sbot")
	v.AutomaticEnv()
	v.BindPFlags(pflag.CommandLine)

	mandatoryArgs := map[string]string{
		"service_cache":                    "Cache",
		"service_vendor_crawl_starts":      "Vendor crawl starts",
		"service_vendor_crawl_finishes":    "Vendor crawl finishes",
		"service_vendor_crawl_unnecessary": "Vendor crawl unnecessary",
	}
	for service, serviceName := range mandatoryArgs {
		if v.GetString(service) == "" {
			return fmt.Errorf("%s service must be set!", serviceName)
		}
	}

	return nil
}

func main() {
	if err := initArgs(os.Args); err != nil {
		log.Fatal("%v", err)
	}

	if err := initLogging(); err != nil {
		log.Fatal("%s", err)
	}
	defer log.Close()

	site := v.GetString("site")
	save := v.GetString("save")
	load := v.GetString("load")
	maxBots := v.GetInt("maxbots")

	config, err := load_config(v.GetString("config"))
	if err != nil {
		log.Fatal("%v", err)
	}

	cache, err := initCache(v.GetString("service_cache"),
		v.GetInt("cache_max"))
	if err != nil {
		log.Fatal("Cannot connect to cache: %v", err)
	}
	defer cache.close()

	if load != "" {
		if f, err := os.Stat(load); err != nil {
			log.Fatal("Problem with load directory '%s': %v",
				load, err)
		} else if !f.IsDir() {
			log.Fatal("'%s' is not a directory", load)
		}
	}
	if save != "" {
		if _, err := os.Stat(save); site == "" && err == nil {
			log.Fatal("Save directory '%s' already exists!", save)
		} else if err := os.MkdirAll(save, 0755); err != nil {
			log.Fatal("Problem creating save directory '%s': %v",
				save, err)
		}

		if load != "" {
			log.Info("-load used.  -save ignored.")
			save = ""
		}
	}

	if site != "" {
		if c, ok := config[site]; !ok {
			log.Fatal("Site '%s' is not in the config file!",
				site)
		} else {
			crawl(site, c, save, load, cache)
		}
	} else {
		siteList := make(chan string, len(config))
		var wg sync.WaitGroup

		max := len(config)
		if max > maxBots {
			max = maxBots
		}
		for i := 0; i < max; i++ {
			wg.Add(1)
			go func(in <-chan string, config map[string]Config) {
				defer wg.Done()
				for name := range in {
					crawl(name, config[name],
						save, load, cache)
				}
			}(siteList, config)
		}

		for name, _ := range config {
			siteList <- name
		}

		close(siteList)
		wg.Wait()
	}

	log.Info("Done crawling")

	PopulateCatalog(ProductCatalog, SoundexCatalog, NysiisCatalog)
}

func load_config(configName string) (map[string]Config, error) {
	var config map[string]Config

	data, err := ioutil.ReadFile(configName)
	if err != nil {
		return config,
			fmt.Errorf("Problem reading config file: %v\n", err)
	}

	err = json.Unmarshal(data, &config)
	if err != nil {
		return config, fmt.Errorf("Problem loading config: %v\n", err)
	}

	return config, nil
}

func initLogging() error {
	envLogLevel := v.GetString("logLevel")
	logLevel, err := log.Str2Level(envLogLevel)
	if err != nil {
		return err
	}

	log.NewDefaultLogger(logLevel)
	return nil
}

func crawl(siteName string, conf Config, save, load string, cache CacheData) {
	startTime := time.Now()
	cData := CrawlData{siteName, conf, save, load, ProductCatalog,
		SoundexCatalog, NysiisCatalog, cache}

	// I don't care much for this returning an error stuff.
	// Maybe we should check in the cases whether we need to
	// crawl or not.
	// Then again, it might be the right approach.  Just doesn't feel right.
	var err error
	switch conf.Type {
	case "itunes":
		err = CrawliTunes(cData)

	case "shopify":
		err = CrawlShopify(cData)

	default:
		log.Info("Cannot crawl %s (%s) yet.", siteName, conf.Type)
		return
	}
	endTime := time.Now()

	if err == nil {
		crawlStarts(siteName, startTime)
		crawlFinishes(siteName, endTime)
	} else if errors.Is(err, CrawlUnnecessary) {
		crawlUnnecessary(siteName, startTime)
	}
}

func crawlStarts(siteName string, when time.Time) {
	event := "service_vendor_crawl_starts"
	err := crawlService(event, siteName, when)
	if err != nil {
		log.Error("Cannot send %s event: %v", event, err)
	}
}

func crawlFinishes(siteName string, when time.Time) {
	event := "service_vendor_crawl_finishes"
	err := crawlService(event, siteName, when)
	if err != nil {
		log.Error("Cannot send %s event: %v", event, err)
	}
}

func crawlUnnecessary(siteName string, when time.Time) {
	event := "service_vendor_crawl_unnecessary"
	err := crawlService(event, siteName, when)
	if err != nil {
		log.Error("Cannot send %s event: %v", event, err)
	}
}

func crawlService(event string, siteName string, when time.Time) error {
	service := v.GetString(event)
	if service == "" {
		log.Fatal("service (%s) cannot be empty!", event)
	}

	whenStr, err := when.MarshalText()
	if err != nil {
		log.Fatal("Problem marshalling time: %v: %s", err, w.WhereAmI)
	}
	formData := url.Values{
		"vendor": {siteName},
		"when":   {string(whenStr)},
	}

	uri := service
	log.Trace("Sending event (%s) for [%s] to %s", event, siteName, uri)
	var resp *http.Response
	err = retry.Do(
		func() error {
			_, err := http.PostForm(uri, formData)
			if err != nil {
				return err
			}
			return nil
		},
		retry.Attempts(5),
		retry.DelayType(retry.BackOffDelay),
		retry.LastErrorOnly(true),
		retry.RetryIf(func(err error) bool {
			return resp == nil || resp.StatusCode >= 500
		}),
	)
	return err
}
