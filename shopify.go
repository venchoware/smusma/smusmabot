package main

import (
	"bufio"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"github.com/antzucaro/matchr"
	golash "github.com/smoke-trees/golash/string"
	log "gitlab.com/venchoware/go/log4go"
	"gonum.org/v1/gonum/stat/distuv"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"
)

type Products []Product
type Product struct {
	Id       int
	Title    string
	Type     string `json:"product_type"`
	Updated  string `json:"updated_at"`
	Variants []Variant
	Images   []Image
	NormalizedTitle
}
type Variant struct {
	Price        string
	PriceUpdated string `json:"updated_at"`
	Available    bool
}
type Image struct {
	Src          string
	Width        int
	Height       int
	ImageUpdated string `json:"updated_at"`
}

const defaultCrawlDelay = 2

func CrawlShopify(cd CrawlData) error {
	for _, collection := range cd.Collections {
		if cd.WantsToLoad() {
			err := cd.loadShopifyCollection(collection)
			if err != nil {
				return err
			}
		} else {
			err := cd.crawlShopifyCollection(collection)
			if err != nil {
				return err
			}
		}
	}
	log.Info("[%s] Done crawling", cd.SiteName)
	return nil
}

func (cd CrawlData) crawlShopifyCollection(coll string) error {
	if cd.collectionNotUpdated(coll) {
		return CrawlUnnecessary
	}

	prefix := "collections/"
	if cd.NotCollections {
		prefix = ""
	}
	endpoint := prefix + coll

	log.Info("[%s] Crawling (%s).\n", cd.SiteName, coll)

	client := &http.Client{}
	firstPage := true
	var crawlDelay time.Duration = defaultCrawlDelay
	var prs distuv.Poisson
	var allProducts Products

	defer log.Info("[%s] Done crawling (%s)", cd.SiteName, coll)

	for page := 1; true; page++ {
		uri := fmt.Sprintf("%s/%s/products.json?page=%1d",
			cd.SiteUrl, endpoint, page)

		if firstPage {
			err := CheckRobot(cd.Config, uri, &crawlDelay)
			if err != nil {
				log.Critical("[%s] %s", cd.SiteName, err)
				return err
			}

			prs = InitPrandomSleepVal(crawlDelay)
			firstPage = false
		}

		// Putting this here so the start is more distributed
		sleepval := PrandomSleepVal(prs)
		log.Trace("[%s] Sleeping %v", cd.SiteName, sleepval)
		time.Sleep(sleepval)

		log.Info("[%s] Getting (%s) page %1d",
			cd.SiteName, coll, page)
		body, err := BodyFetch(client, uri, cd.SiteName)
		if err == LastPage {
			break
		} else if err == TooManyRequests {
			return err
		} else if err != nil {
			log.Error("[%s] Problem getting (%s) page %1d",
				cd.SiteName, coll, page)
			return err
		}

		var products map[string]Products
		err = json.Unmarshal(body, &products)
		if err != nil {
			log.Error("[%s] Problem decoding JSON for (%s) page %1d",
				cd.SiteName, coll, page)
			return err
		}

		if len(products["products"]) == 0 {
			break
		}

		if cd.WantsToSave() {
			allProducts = append(allProducts, products["products"]...)
		} else {
			cd.processShopify(products["products"])
		}
	}

	if cd.WantsToSave() {
		cd.saveShopifyCollection(coll, allProducts)
	}

	return nil
}

func (cd CrawlData) processShopify(products []Product) {
	for _, product := range products {
		if !product.Variants[0].Available {
			continue
		}

		log.Info("[%s] Processing '%s' for %s.", cd.SiteName,
			product.Title, product.Variants[0].Price)

		err := cd.chew(&product)
		if err != nil {
			log.Warn("[%s] %s", cd.SiteName, err)
			break
		}

		if product.MainTitle != "" {
			AddToCatalog(product.NormalizedTitle)
		}
	}
}

func (cd CrawlData) chew(product *Product) error {
	siteRegexes := map[string]string{
		"bosscodes":                `^(.+?)\s+((?:VUDU|MA|iTunes|ITUNES|Vudu|Google).*)$`,
		"buydigitalmovie":          `^(.+?)\s+\((.+[\]\)])\s*$`,
		"casavideocodes":           `^(.+?)\s+((?:\(?\s*[HS]D\s*\)?)?\s*\(?\s*\d+/\d+\)?.*)$`,
		"digitaladdictsanonymous":  `^(.+?)\s+([\[\(].+)$`,
		"digitalmoviecodesandmore": `^(.+?)\s+((?:VUDU|Vudu|vudu|HD|SD|4K|ITUNES|Instawatch).*)$`,
		"digitalmoviesnow":         `^(.+?)\s+((?:HD|UV|\(Vudu|\(?[iI]-?[tT]unes|\(IT\)).*)$`,
		"fastflicks":               `^(.+?)\s+(\[.+\])\s*$`,
		"happywatching":            `^(.+?)\s+((?:HD|SD|4K|\(Redeem|iTunes|XML|\(Choose).*)$`,
		// This site has many titles with no format info
		"hdmoviecodes":            `^(.+?)\s+((?:HD|Google Play|iTunes|VUDU|UV|MA).*)$`,
		"michaels-digital-movies": `^(.+?)\s*\[(.+?)(?:(?:\].*)|(?:[^\]]*))$`,
		"newreleasecodes":         `^(.+?)\s+((?:HD|SD|4[kK]|Disney Reward).*)$`,
		"nicksverified":           `^(.+?)\s+((?:HD|SD|4[kK]|iTunes|UHD|Vudu|Google Play).*)$`,
		"onestopflixshop":         `^(.+?)\s*\((.+?)\)?\s*$`,
		"thefastflicks":           `^(.+?)\s+(\[.+\])\s*$`,
		// This site has a few titles with no format info
		"uvcodeshop": `^(.+?)\s+((?:HD|SD|UV|4[kK]|\(?[iI][tT][uU]|\(GOOGLE|\(MOVIES|INSTAWATCH).*)$`,
		"uvdigimart": `^(.+?)\s+((?:VUDU|MA|iTunes|ITUNES|Vudu|Google|UV|Copy Download|Digital Copy).*)$`,
		"vifgam":     `^(.+?)\s*\((.+)\)\s*$`,
	}

	regexStr, ok := siteRegexes[cd.SiteName]
	if !ok {
		return fmt.Errorf("cannot chew products for '%s' yet",
			cd.SiteName)
	}

	regex, err := regexp.Compile(regexStr)
	if err != nil {
		log.Critical("Problem compiling fullLine regex: %v", err)
		return err
	}

	matches := regex.FindAllStringSubmatch(product.Title, -1)
	if len(matches) != 1 || len(matches[0]) != 3 {
		log.Trace("[%s] Title mismatch: '%s'",
			cd.SiteName, product.Title)
	} else {
		title := matches[0][1]
		detail := matches[0][2]
		log.Trace("[%s] Title is '%s', format is in '%s'",
			cd.SiteName, title, detail)

		product.NormalizedTitle = cd.normalize(title)
		log.Trace("[%s] Normalized title is '%s' (%s)",
			cd.SiteName, product.MainTitle, product.MainSoundex)
	}

	return nil
}

func (cd CrawlData) saveShopifyCollection(coll string, products Products) error {
	fname := fmt.Sprintf("%s/%s-%s", cd.SavePath(), cd.SiteName, coll)
	mode := os.O_WRONLY | os.O_CREATE | os.O_APPEND
	f, err := os.OpenFile(fname, mode, 0666)
	if err != nil {
		log.Critical("[%s] Cannot open %s for writing: %v",
			cd.SiteName, fname, err)
		return err
	}
	defer f.Close()

	fp := bufio.NewWriter(f)
	defer fp.Flush()

	data, err := json.MarshalIndent(products, "", "  ")
	if err != nil {
		log.Error("[%s] Problem encoding for writing: %v",
			cd.SiteName, err)
		return err
	}
	fmt.Fprintf(fp, "%s\n", data)

	return nil
}

func (cd CrawlData) loadShopifyCollection(coll string) error {
	fname := fmt.Sprintf("%s/%s-%s", cd.LoadPath(), cd.SiteName, coll)
	dat, err := ioutil.ReadFile(fname)
	if err != nil {
		log.Error("[%s] Problem loading '%s': %v",
			cd.SiteName, fname, err)
		return err
	}

	var products Products
	err = json.Unmarshal(dat, &products)
	if err != nil {
		log.Error("[%s] Problem decoding JSON for (%s): %v",
			cd.SiteName, coll, err)
		return err
	}

	cd.processShopify(products)

	return nil
}

var frontTosser,
	backTosser,
	puncStripper1,
	puncStripper2,
	spaceStripper,
	extraCruft,
	allDigits,
	romanNumerals,
	blanks *regexp.Regexp

func init() {
	var err error

	frontTosser, err = regexp.Compile(`^(?:the|a|an)\s+(.+)$`)
	if err != nil {
		log.Critical("Problem compiling frontTosser regex: %v", err)
		panic(err)
	}

	backTosser, err = regexp.Compile(`^(.+),\s*(?:the|a|an)$`)
	if err != nil {
		log.Critical("Problem compiling backTosser regex: %v", err)
		panic(err)
	}

	puncStripper1, err = regexp.Compile(`[’\']`)
	if err != nil {
		log.Critical("Problem compiling puncStripper regex: %v", err)
		panic(err)
	}

	puncStripper2, err = regexp.Compile(`[^\w\s]`)
	if err != nil {
		log.Critical("Problem compiling puncStripper regex: %v", err)
		panic(err)
	}

	spaceStripper, err = regexp.Compile(`\s{2,}`)
	if err != nil {
		log.Critical("Problem compiling spaceStripper regex: %v", err)
		panic(err)
	}

	extraCruft, err = regexp.Compile(`(\s+(?:hd|sd|itunes|uv|vudu))`)
	if err != nil {
		log.Critical("Problem compiling spaceStripper regex: %v", err)
		panic(err)
	}

	blanks, err = regexp.Compile(`\s+`)
	if err != nil {
		log.Critical("Problem compiling blanks regex: %v", err)
		panic(err)
	}

	allDigits, err = regexp.Compile(`^\d+$`)
	if err != nil {
		log.Critical("Problem compiling allDigits regex: %v", err)
		panic(err)
	}

	romanNumerals, err = regexp.Compile(`^iii?|iv|v|vi+|ix|x|xi+|xiv|xv|xvi+$`)
	if err != nil {
		log.Critical("Problem compiling romanNumerals regex: %v", err)
		panic(err)
	}
}

// https://www.addaptive.com/blog/using-algorithms-normalize-company-names/
func (cd CrawlData) normalize(title string) NormalizedTitle {
	results := NormalizedTitle{}

	// oldWriter := log.Writer()
	// defer log.SetOutput(oldWriter)
	// log.SetOutput(ioutil.Discard)

	title = strings.TrimSpace(title)
	log.Fine("[%s] Stripped title: '%s'", cd.SiteName, title)

	title = strings.ToLower(golash.Deburr(title))
	results.AltTitle = title
	log.Fine("[%s] Deburred title: '%s'", cd.SiteName, title)

	deburredSoundexCode := cd.getSoundexCode(title)
	results.AltSoundex = deburredSoundexCode
	log.Fine("[%s] Soundex deburr: '%s'", cd.SiteName, deburredSoundexCode)
	deburredNysiisCode := cd.getNysiisCode(title)
	results.AltNysiis = deburredNysiisCode
	log.Fine("[%s] Nysiis deburr: '%s'", cd.SiteName, deburredNysiisCode)

	title = string(frontTosser.ReplaceAll([]byte(title), []byte("$1")))
	log.Fine("[%s] Front-tossed title: '%s'", cd.SiteName, title)

	title = string(backTosser.ReplaceAll([]byte(title), []byte("$1")))
	log.Fine("[%s] Back-tossed title: '%s'", cd.SiteName, title)

	title = string(puncStripper1.ReplaceAll([]byte(title), []byte("")))
	log.Fine("[%s] Punctuation-stripped1 title: '%s'", cd.SiteName, title)

	title = string(puncStripper2.ReplaceAll([]byte(title), []byte(" ")))
	log.Fine("[%s] Punctuation-stripped2 title: '%s'", cd.SiteName, title)

	title = strings.TrimSpace(title)
	log.Fine("[%s] Stripped2 title: '%s'", cd.SiteName, title)

	title = string(spaceStripper.ReplaceAll([]byte(title), []byte(" ")))
	results.MainTitle = title
	log.Fine("[%s] Space-stripped title: '%s'", cd.SiteName, title)

	title = string(extraCruft.ReplaceAll([]byte(title), []byte("")))
	results.MainTitle = title
	log.Fine("[%s] Cruft-stripped title: '%s'", cd.SiteName, title)

	soundexCode := cd.getSoundexCode(title)
	results.MainSoundex = soundexCode
	log.Fine("[%s] Soundex title: '%s'", cd.SiteName, soundexCode)

	nysiisCode := cd.getNysiisCode(title)
	results.MainNysiis = nysiisCode
	log.Fine("[%s] Nysiis title: '%s'", cd.SiteName, nysiisCode)

	return results
}

// Duplication is evil.  Fix these.
func (cd CrawlData) getSoundexCode(title string) string {
	words := blanks.Split(title, -1)
	nofWords := len(words)
	if nofWords == 1 {
		return ""
	}

	var soundexes []string
	for _, word := range words {
		if allDigits.Match([]byte(word)) || romanNumerals.Match([]byte(word)) {
			soundexes = append(soundexes, word)
			nofWords--
		} else {
			soundexes = append(soundexes, matchr.Soundex(word))
		}
	}

	if nofWords <= 1 {
		return ""
	}

	return strings.Join(soundexes, " ")
}

func (cd CrawlData) getNysiisCode(title string) string {
	words := blanks.Split(title, -1)
	nofWords := len(words)
	if nofWords == 1 {
		return ""
	}

	var soundexes []string
	for _, word := range words {
		if allDigits.Match([]byte(word)) || romanNumerals.Match([]byte(word)) {
			soundexes = append(soundexes, word)
			nofWords--
		} else {
			soundexes = append(soundexes, matchr.NYSIIS(word))
		}
	}

	if nofWords <= 1 {
		return ""
	}

	return strings.Join(soundexes, " ")
}

func (cd CrawlData) collectionNotUpdated(coll string) bool {
	client := &http.Client{}
	sitemap_url := fmt.Sprintf("%s/sitemap_collections_1.xml", cd.SiteUrl)

	var sitemap struct {
		XMLName     xml.Name `xml:"urlset"`
		Collections []struct {
			Collection string `xml:"loc"`
			LastUpdate string `xml:"lastmod"`
		} `xml:"url"`
	}

	data, err := BodyFetch(client, sitemap_url, cd.SiteName)
	if err != nil {
		log.Error("[%s] Problem getting (%s) sitemap",
			cd.SiteName, coll)
		return true
	}

	err = xml.Unmarshal([]byte(data), &sitemap)
	if err != nil {
		log.Error("[%s] Cannot process sitemap catalog (%s): %v",
			cd.SiteName, coll, err)
		return true
	}

	regex, err := regexp.Compile(`/` + coll + `$`)
	if err != nil {
		log.Critical("Problem compiling fullLine regex: %v", err)
		panic(err)
	}

	for _, collection := range sitemap.Collections {
		if !regex.Match([]byte(collection.Collection)) {
			continue
		}

		lastUpdate, err := time.Parse(`2006-01-02T15:04:05-07:00`,
			collection.LastUpdate)
		if err != nil {
			log.Critical("[%s] Problem parsing last update: %v", err)
			return false
		}

		lastCrawl, err := cd.Cache.getCrawlLastFinish(cd.SiteName)
		if err != nil {
			log.Error("Problem getting last finish (%s): %v",
				cd.SiteName, err)
			return false
		}

		log.Trace("[%s] Url: last update (%s): %v > %v?",
			cd.SiteName, collection.Collection,
			lastUpdate, lastCrawl)
		if !lastUpdate.After(lastCrawl) {
			log.Info("[%s] Collection %s not updated recently",
				cd.SiteName, coll)
			break
		} else {
			return false
		}
	}

	return true
}
