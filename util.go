package main

import (
	"errors"
	"fmt"
	retry "github.com/avast/retry-go"
	"github.com/temoto/robotstxt"
	log "gitlab.com/venchoware/go/log4go"
	"golang.org/x/exp/rand"
	"gonum.org/v1/gonum/stat/distuv"
	"io/ioutil"
	"net/http"
	"sync"
	"time"
)

type Config struct {
	Type           string
	SiteUrl        string
	Endpoint       string
	NotCollections bool
	Collections    []string
}

type CrawlData struct {
	SiteName string
	Config
	Save, Load     string
	ProductCatalog Catalog
	SoundexCatalog Catalog
	NysiisCatalog  Catalog
	Cache          CacheData
}

type NormalizedTitle struct {
	MainTitle   string
	MainSoundex string
	MainNysiis  string
	AltTitle    string
	AltSoundex  string
	AltNysiis   string
}

func (c CrawlData) LoadPath() string {
	return c.Load
}

func (c CrawlData) SavePath() string {
	return c.Save
}

func (c CrawlData) WantsToLoad() bool {
	return c.Load != ""
}

func (c CrawlData) WantsToSave() bool {
	return c.Save != ""
}

type Catalog map[string]string

var (
	mu             sync.Mutex
	ProductCatalog = make(Catalog)
	SoundexCatalog = make(Catalog)
	NysiisCatalog  = make(Catalog)
)

func AddToCatalog(nt NormalizedTitle) {
	mu.Lock()
	ProductCatalog[nt.MainTitle] = nt.MainTitle
	SoundexCatalog[nt.MainTitle] = nt.MainSoundex
	NysiisCatalog[nt.MainTitle] = nt.MainNysiis
	mu.Unlock()
}

const userAgent = "Venchoware-SmusmaBot (bot@smusma.com)"

var LastPage error = errors.New("last page")
var TooManyRequests error = errors.New("too many requests")
var CrawlUnnecessary error = errors.New("unnecessary")

func CheckRobot(s Config, uri string, crawlDelay *time.Duration) error {
	robotUrl := fmt.Sprintf("%s/robots.txt", s.SiteUrl)
	resp, err := fetch(nil, robotUrl)
	if err != nil {
		return fmt.Errorf("Problem getting robots.txt: %v", err)
	}
	defer resp.Body.Close()

	robot, err := robotstxt.FromResponse(resp)
	if err != nil {
		return fmt.Errorf("Problem processing robots.txt: %v", err)
	}

	group := robot.FindGroup(userAgent)
	if !group.Test(uri) {
		return errors.New("robots.txt has excluded us!")
	}

	if group.CrawlDelay > 0 {
		*crawlDelay = group.CrawlDelay
		log.Info("Setting crawl delay to %v per robots.txt",
			*crawlDelay)
	}

	return nil
}

func BodyFetch(client *http.Client, uri, siteName string) ([]byte, error) {
	for tries := 0; tries < 3; tries++ {
		if tries > 0 {
			sleepval := time.Duration(90+tries*60) * time.Second
			log.Trace("[%s] Sleeping %v", siteName, sleepval)
			time.Sleep(sleepval)
		}

		resp, err := fetch(client, uri)
		if err != nil {
			log.Error("[%s] Problem with '%s: %s",
				siteName, uri, err)
			return nil, err
		}
		defer resp.Body.Close()

		switch resp.StatusCode {
		case http.StatusTooManyRequests, 430:
			log.Info("[%s] Too many requests.", siteName)
		case http.StatusOK:
			return ioutil.ReadAll(resp.Body)
		default:
			log.Error("[%s] Not OK: %s (%1d)", siteName,
				http.StatusText(resp.StatusCode),
				resp.StatusCode)
			return nil, LastPage
		}
	}
	return nil, TooManyRequests
}

func InitPrandomSleepVal(secs time.Duration) distuv.Poisson {
	// Don't want to do all this for each page.
	seed := uint64(time.Now().UTC().UnixNano())
	return distuv.Poisson{float64(secs), rand.NewSource(seed)}
}

func PrandomSleepVal(p distuv.Poisson) time.Duration {
	sleepval := p.Rand()
	if sleepval == 0 {
		sleepval = 1
	}
	return time.Duration(sleepval) * time.Second
}

func fetch(client *http.Client, uri string) (*http.Response, error) {
	if client == nil {
		client = &http.Client{}
	}

	var resp *http.Response
	err := retry.Do(
		func() error {
			var err error
			req, err := http.NewRequest("GET", uri, nil)
			if err != nil {
				return err
			}
			req.Header.Add("User-Agent", userAgent)

			resp, err = client.Do(req)
			return err
		},
		retry.Attempts(5),
		retry.DelayType(retry.BackOffDelay),
		retry.LastErrorOnly(true),
		retry.RetryIf(func(err error) bool {
			return resp.StatusCode >= 500
		}),
	)
	return resp, err
}
