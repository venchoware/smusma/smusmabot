package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	log "gitlab.com/venchoware/go/log4go"
	"io/ioutil"
	"net/http"
	"os"
)

type Feed struct {
	Results []Result
}

type Result struct {
	Name    string
	Kind    string
	Artwork string `json:"artworkUrl100"`
	Url     string
}

func CrawliTunes(cd CrawlData) error {
	log.Info("Crawling %s.\n", cd.SiteName)

	if cd.WantsToLoad() {
		cd.loadiTunes()
		return nil
	}

	client := &http.Client{}
	body, err := BodyFetch(client, cd.SiteUrl, cd.SiteName)
	if err != nil {
		log.Error("[%s] Problem fetching %s: %v", cd.SiteName, err)
		return err
	}

	var feed map[string]Feed
	err = json.Unmarshal(body, &feed)
	if err != nil {
		log.Error("[%s] Problem decoding JSON for %s: %v",
			cd.SiteName, err)
		return err
	}

	results := feed["feed"].Results
	if len(results) == 0 {
		log.Warn("[%s] No results for %s: %v", cd.SiteName, err)
		return err
	}

	if cd.WantsToSave() {
		cd.save(results)
	} else {
		cd.processiTunes(results)
	}

	log.Info("[%s] Done crawling", cd.SiteName)
	return nil
}

func (cd CrawlData) processiTunes(results []Result) {
	for _, result := range results {
		// <meta name="description" content="... for $xx.yy."
		price := "??.??"

		log.Info("[%s] Processing %s for %s",
			cd.SiteName, result.Name, price)
	}
}

func (cd CrawlData) save(results []Result) error {
	fname := fmt.Sprintf("%s/%s", cd.SavePath(), cd.SiteName)
	mode := os.O_WRONLY | os.O_CREATE | os.O_APPEND
	f, err := os.OpenFile(fname, mode, 0666)
	if err != nil {
		log.Critical("[%s] Cannot open %s for writing: %v",
			cd.SiteName, fname, err)
		return err
	}
	defer f.Close()

	fp := bufio.NewWriter(f)
	defer fp.Flush()

	data, err := json.MarshalIndent(results, "", "  ")
	if err != nil {
		log.Critical("[%s] Problem encoding for writing: %v",
			cd.SiteName, err)
		return err
	}
	fmt.Fprintf(fp, "%s\n", data)

	return nil
}

func (cd CrawlData) loadiTunes() error {
	fname := fmt.Sprintf("%s/%s", cd.LoadPath(), cd.SiteName)
	dat, err := ioutil.ReadFile(fname)
	if err != nil {
		log.Error("[%s] Problem loading '%s': %v",
			cd.SiteName, fname, err)
		return err
	}

	var results []Result
	err = json.Unmarshal(dat, &results)
	if err != nil {
		log.Error("[%s] Problem decoding JSON: %v", cd.SiteName, err)
		return err
	}

	cd.processiTunes(results)

	return nil
}
