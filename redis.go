package main

import (
	retry "github.com/avast/retry-go"
	"github.com/mediocregopher/radix"
	"time"
)

type CacheData struct {
	Client *radix.Pool
}

func initCache(service string, max int) (CacheData, error) {
	var cache CacheData
	err := retry.Do(
		func() error {
			var err error
			cache.Client, err = radix.NewPool("tcp", service, max)
			if err != nil {
				return err
			}
			return nil
		},
		retry.Attempts(5),
		retry.DelayType(retry.BackOffDelay),
		retry.LastErrorOnly(true),
		retry.RetryIf(func(err error) bool {
			return err != nil
		}),
	)

	return cache, err
}

func (cache CacheData) getCrawlLastFinish(vendor string) (time.Time, error) {
	var finish string
	err := cache.Client.Do(radix.Cmd(&finish, "GET", vendor))
	if err != nil {
		return time.Now(), err
	}

	return time.Parse(time.RFC3339, finish)
}

func (cache CacheData) close() error {
	return cache.Client.Close()
}
