package main

import (
	log "gitlab.com/venchoware/go/log4go"
)

var titles = make(Catalog)
var soundexes = make(Catalog)
var nysiises = make(Catalog)

func PopulateCatalog(rawTitles, rawSoundexes, rawNysiises Catalog) {
	for title, _ := range rawTitles {
		if _, ok := titles[title]; ok {
			log.Trace("Easy match for '%s'", title)
			continue
		}

		soundex := rawSoundexes[title]
		nysiis := rawNysiises[title]

		// } else if sm, ok := titles[soundexes[soundex]]; soundex != "" && ok {
		if ok, match := soundexesMatch(title, soundex, nysiis); ok {
			log.Trace("Soundex match on '%s' matched '%s'",
				title, match)
			titles[title] = match
		} else {
			log.Trace("Adding '%s' to the titles", title)
			titles[title] = title
			soundexes[soundex] = title
			nysiises[nysiis] = title
		}
	}
}

func soundexesMatch(title, soundex, nysiis string) (bool, string) {
	if soundex == "" {
		return false, ""
	}

	sm, sok := titles[soundexes[soundex]]
	nm, nok := titles[nysiises[nysiis]]
	if sok && nok && sm == nm {
		return true, sm
	}

	return false, ""
}
