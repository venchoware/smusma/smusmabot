package main

import (
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"testing"
)

func TestNoArgs(t *testing.T) {
	noArgs := []string{"smusmabot"}
	pflag.CommandLine = pflag.NewFlagSet(noArgs[0], pflag.ExitOnError)
	v = viper.New()
	if err := initArgs(noArgs); err == nil {
		t.Errorf("Did not catch missing args: %v", err)
	}
}

func TestAllArgs(t *testing.T) {
	allArgs := []string{
		"smusmabot",
		"--service_cache", "john",
		"--service_vendor_crawl_starts", "paul",
		"--service_vendor_crawl_finishes", "george",
		"--service_vendor_crawl_unnecessary", "ringo",
	}
	pflag.CommandLine = pflag.NewFlagSet(allArgs[0], pflag.ExitOnError)
	v = viper.New()
	if err := initArgs(allArgs); err != nil {
		t.Errorf("All args should pass: %v", err)
	}
}

func TestMissingCache(t *testing.T) {
	allArgs := []string{
		"smusmabot",
		"--service_vendor_crawl_starts", "paul",
		"--service_vendor_crawl_finishes", "george",
		"--service_vendor_crawl_unnecessary", "ringo",
	}
	pflag.CommandLine = pflag.NewFlagSet(allArgs[0], pflag.ExitOnError)
	v = viper.New()
	if err := initArgs(allArgs); err == nil {
		t.Errorf("Missed missing cache arg")
	}
}

func TestMissingStart(t *testing.T) {
	allArgs := []string{
		"smusmabot",
		"--service_cache", "john",
		"--service_vendor_crawl_finishes", "george",
		"--service_vendor_crawl_unnecessary", "ringo",
	}
	pflag.CommandLine = pflag.NewFlagSet(allArgs[0], pflag.ExitOnError)
	v = viper.New()
	if err := initArgs(allArgs); err == nil {
		t.Errorf("Missed missing start service arg")
	}
}

func TestMissingFinish(t *testing.T) {
	allArgs := []string{
		"smusmabot",
		"--service_cache", "john",
		"--service_vendor_crawl_starts", "paul",
		"--service_vendor_crawl_unnecessary", "ringo",
	}
	pflag.CommandLine = pflag.NewFlagSet(allArgs[0], pflag.ExitOnError)
	v = viper.New()
	if err := initArgs(allArgs); err == nil {
		t.Errorf("Missed missing finish service arg")
	}
}

func TestMissingUnnecessary(t *testing.T) {
	allArgs := []string{
		"smusmabot",
		"--service_cache", "john",
		"--service_vendor_crawl_starts", "paul",
		"--service_vendor_crawl_finishes", "george",
	}
	pflag.CommandLine = pflag.NewFlagSet(allArgs[0], pflag.ExitOnError)
	v = viper.New()
	if err := initArgs(allArgs); err == nil {
		t.Errorf("Missed missing unnecessary service arg")
	}
}
