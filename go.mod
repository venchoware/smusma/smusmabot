module gitlab.com/venchoware/smusma/smusmabot

go 1.14

require (
	github.com/antzucaro/matchr v0.0.0-20191224151129-ab6ba461ddec
	github.com/avast/retry-go v2.6.0+incompatible
	github.com/smoke-trees/golash v0.0.0-20191019170324-0895e8b1cf92
	github.com/spf13/viper v1.6.2
	github.com/temoto/robotstxt v1.1.1
	github.com/toolkits/file v0.0.0-20160325033739-a5b3c5147e07 // indirect
	gitlab.com/venchoware/go/log4go v0.0.0-20200405194344-21a3aa52371c
	golang.org/x/exp v0.0.0-20200331195152-e8c3332aa8e5
	gonum.org/v1/gonum v0.7.0
)
